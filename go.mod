module bitbucket.org/sikharis/confra

go 1.13

require (
	bitbucket.org/sikharis/envix v1.0.7
	github.com/bhendo/go-powershell v0.0.0-20190719160123-219e7fb4e41e // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gookit/color v1.2.0
	github.com/juju/errors v0.0.0-20190930114154-d42613fe1ab9 // indirect
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20190723135506-ce30eb24acd2 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.1.10
	golang.org/x/crypto v0.0.0-20190927123631-a832865fa7ad // indirect
	golang.org/x/net v0.0.0-20190926025831-c00fd9afed17 // indirect
	golang.org/x/sys v0.0.0-20190927073244-c990c680b611 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
