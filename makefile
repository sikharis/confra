# Build And Development
.DEFAULT_GOAL := run

run:	
	@go build -o confra.exe
	@confra.exe

modinfo:
	@go list -m all

tidy:
	@go mod tidy