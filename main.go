package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"runtime"

	"bitbucket.org/sikharis/envix"
	"github.com/gookit/color"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Config struct {
	Credential  Credential    `json:"credential"`
	Environment []Environment `json:"environment"`
}
type Credential struct {
	User     string `json:"user"`
	Password string `json:"password"`
}
type Endpoints struct {
	Address  string
	Protocol string `json:"protocol"`
	Path     string `json:"path"`
}
type Services struct {
	Name string `json:"name"`
}
type Environment struct {
	Type       string      `json:"type"`
	Name       string      `json:"name"`
	Address    string      `json:"address"`
	Credential Credential  `json:"credential"`
	Endpoints  []Endpoints `json:"endpoints"`
	Services   []Services  `json:"services"`
}

type Remote struct {
	Service envix.Service
}

func main() {

	fmt.Println(`
██████╗ ██████╗ ███╗   ██╗███████╗██████╗  █████╗ 
██╔════╝██╔═══██╗████╗  ██║██╔════╝██╔══██╗██╔══██╗
██║     ██║   ██║██╔██╗ ██║█████╗  ██████╔╝███████║
██║     ██║   ██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║
╚██████╗╚██████╔╝██║ ╚████║██║     ██║  ██║██║  ██║
╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝

CONFINS Infrastructure Application ver.1.0.0

`)

	//Read configuration file
	file, _ := ioutil.ReadFile("env.json")
	c := Config{}
	err := json.Unmarshal([]byte(file), &c)
	isError(err)

	// Echo instance
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// fmt.Printf("%v", c)
	//Worker process
	worker(&c)
	// var svc envix.Services = 1

	// info, _ := svc.Info()
	// fmt.Printf("Service \"%s\" is %t\n", svc, info.Active)

	// Routes
	// e.GET("/", index)

	// openbrowser("http://localhost:1111/")

	// Start server
	// e.Logger.Fatal(e.Start(":1111"))
}

func index(c echo.Context) error {
	return c.HTML(http.StatusOK, `<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
	<title>CONFINS Checklist</title>
	<script>!function(t,e){function n(t){return t&&e.XDomainRequest&&!/MSIE 1/.test(navigator.userAgent)?new XDomainRequest:e.XMLHttpRequest?new XMLHttpRequest:void 0}function o(t,e,n){t[e]=t[e]||n}var r=["responseType","withCredentials","timeout","onprogress"];t.ajax=function(t,a){function s(t,e){return function(){c||(a(void 0===f.status?t:f.status,0===f.status?"Error":f.response||f.responseText||e,f),c=!0)}}var u=t.headers||{},i=t.body,d=t.method||(i?"POST":"GET"),c=!1,f=n(t.cors);f.open(d,t.url,!0);var l=f.onload=s(200);f.onreadystatechange=function(){4===f.readyState&&l()},f.onerror=s(null,"Error"),f.ontimeout=s(null,"Timeout"),f.onabort=s(null,"Abort"),i&&(o(u,"X-Requested-With","XMLHttpRequest"),e.FormData&&i instanceof e.FormData||o(u,"Content-Type","application/x-www-form-urlencoded"));for(var p,m=0,v=r.length;v>m;m++)p=r[m],void 0!==t[p]&&(f[p]=t[p]);for(var p in u)f.setRequestHeader(p,u[p]);return f.send(i),f},e.nanoajax=t}({},function(){return this}());</script>
	<style>.datagrid table { border-collapse: collapse; text-align: left; width: 100%; } .datagrid {font: normal 12px/150% Arial, Helvetica, sans-serif; background: #fff; overflow: hidden; border: 1px solid #006A9E; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px; }.datagrid table td, .datagrid table th { padding: 6px 10px; }.datagrid table thead th {background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #0096C4), color-stop(1, #00557F) );background:-moz-linear-gradient( center top, #0096C4 5%, #00557F 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0096C4', endColorstr='#00557F');background-color:#0096C4; color:#FFFFFF; font-size: 14px; font-weight: bold; } .datagrid table thead th:first-child { border: none; }.datagrid table tbody td { color: #00537A; font-size: 13px;border-bottom: 1px solid #6A95BA;font-weight: normal; }.datagrid table tbody td:first-child { border-left: none; }.datagrid table tbody tr:last-child td { border-bottom: none; }</style>
	</head>
	
	<body>
	<div class="datagrid"><table>
	<thead><tr><th>header</th><th>header</th><th>header</th><th>header</th></tr></thead>
	<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
	<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
	<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
	<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
	<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
	</tbody>
	</table></div>
	</body>
	
	</html>`)
}

func worker(data *Config) {

	color.Printf(" %-10s %-20s %-19s %-20s\n", "TYPE", "ALIAS", "HOST/IP", "SERVICE STATUS")

	for _, env := range data.Environment {
		env.showHeader()
		for _, svc := range env.Services {
			// fmt.Printf("%v %v \n\n", env.Address, svc.Name)
			res, err := envix.Services{
				Service: svc.getService(),
				Target: envix.Target{
					Address:  env.Address,
					User:     env.Credential.User,
					Password: env.Credential.Password,
				},
			}.Get()
			fmt.Printf("%v", res.Query.Command)
			env.showStatus(res.Active, string(res.Query.Result), err, "SERVICE: "+svc.Name)
		}

		for _, svc := range env.Endpoints {
			svc.Address = env.Address
			fullURL, status, output, err := svc.checkURL()
			env.showStatus(status, output, err, "ENDPOINT: "+fullURL)
		}
		env.showFooter()
	}

	fmt.Print("Press 'Enter' to continue...")
	bufio.NewReader(os.Stdin).ReadBytes('\n')
}

func (svc Services) getService() envix.Service {
	switch svc.Name {
	case "MSDTC":
		return envix.MSDTC

	case "MSMQ":
		return envix.MSMQ
	default:
		return envix.UNKNOWN
	}
}

//https://gist.github.com/hyg/9c4afcd91fe24316cbf0
func openbrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}

}

func isError(err error, msg ...string) bool {
	if err != nil {
		color.New(color.FgWhite, color.BgRed).Println(fmt.Sprint(err.Error()) + ": " + fmt.Sprintf("%v", err))
	}

	return (err != nil)
}

func (e *Endpoints) checkURL() (FullURL string, Status bool, Output string, Error error) {
	url := fmt.Sprintf("%s://%s/%s", e.Protocol, e.Address, e.Path)
	resp, err := http.Get(url)

	status := false
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		status = true
	}

	output := fmt.Sprintf("%b - %s", resp.StatusCode, http.StatusText(resp.StatusCode))

	return url, status, output, err
}

func (e *Environment) showStatus(status bool, output string, err error, Tag string) {
	fmt.Println()
	if status {
		color.BgGreen.Printf(" %s ", Tag)
	} else {
		color.BgRed.Printf(" %s ", Tag)
	}
	fmt.Print(" ")
}

func (e *Environment) showHeader() {
	// color.Printf("\n %-10s %-20s %-20s", e.Type, e.Name, e.Address)
	color.Printf(`\n##### %s - %s #####S`, e.Type, e.Name, e.Address)
}

func (e *Environment) showFooter() {
	fmt.Printf("\n\n")
}

// // func deleteFile(path string) {
// // 	var err = os.Remove(path)
// // 	if isError(err) {
// // 		return
// // 	}

// // 	fmt.Println("==> done deleting file")
// // }

// func (r *Remote) showStatus(status bool, output string, err error, Tag string) {
// 	if status {
// 		color.BgGreen.Printf(" %s ", Tag)
// 	} else {
// 		color.BgRed.Printf(" %s ", Tag)
// 	}
// 	fmt.Print(" ")
// }
